(defsystem :slice-media
  ;; :default-component-class cl-source-file.cl
  :description "A simple Common Lisp package to slice, splice, and combine media files."
  :version "0.0"
  :author "Udyant Wig <udyant.wig@gmail.com>"
  :licence "Undecided"
  :depends-on ("fiveam" "bluewolf-utilities")
  :components ((:file "slice-media")))
