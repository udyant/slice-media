;;;; -*- mode: common-lisp -*-
(declaim (optimize safety))

(defpackage "SLICE-MEDIA"
  (:documentation "Slice, splice, and combine media files")
  (:use "CL")
  (:use "IT.BESE.FIVEAM")
  (:use "BLUEWOLF-UTILITIES")
  (:export "PRINT-SECONDS-AS-TIME-STRING"
	   "SPEED-VIDEO"
	   "SLICE-MEDIA"))
(in-package "SLICE-MEDIA")

;;; A simple Common Lisp package to slice, splice, and combine media files.
;;; Requires ffmpeg(1) to be installed.
;;; N.B. Developed and tested originally on CMUCL and SBCL.  Later CCL.
;;; Provisional support for Allegro CL has been added recently.
;;;
;;; Usage:
;;;    (slice-media :source <path to source media file>
;;;                 :destination <path to destination media file>
;;;                 :timings <path to timings file>)
;;;
;;; With each source media file there is a file of timings for each slice, i.e.,
;;; the time when the slice begins, and the time when it ends.  Each time is
;;; represented as a triplet of three numbers, which is a list of the hours, the
;;; minutes, and the seconds.  E.g.  (01 20 44) Note that the seconds value may
;;; be fractional, to account for milliseconds, so that we may have (01 20
;;; 44.850)
;;;
;;; Thus, each start time and end time is such a triplet.  We can then combine
;;; these two times together in a list.
;;; E.g.
;;;     ((01 20 44) (01 30 45))
;;;
;;; And now, if a media file will have to be sliced up at many points, the list
;;; of times becomes, for example,
;;;     (((01 20 44) (01 30 45))
;;;      ((01 32 12.350) (01 33 13.850)))
;;;
;;; However, to compute with, triplets may become unwieldy.  Consequently, after
;;; we obtain a triplet, we can convert it to a time-slice structure.
;;;
;;; Each triplet corresponds to an ffmpeg(1) command string.  A triplet-pair
;;;    (list (list 00 01 02) (list 00 03 04))
;;; corresponds to the command string
;;;    "ffmpeg -i <source media file> -ss <time-string of first triplet> -t <time-string of difference> -c:v <video codec> -c:a <audio codec> <intermediate file #n>"
;;; The source media file will be taken as the first argument, the time-string
;;; of the first triplet will be produced by a function, the time-string of the
;;; difference is that corresponding to the length of the time between the start
;;; and end times of the slice in question, the intermediate file #n is a
;;; concatenation of the *INTERMEDIATE-PREFIX* with the rank of the slice in
;;; question; the extension is the containing format, say '.mp4'

(def-suite slice-media-test-suite :description "Test SLICE-MEDIA.")
(in-suite slice-media-test-suite)

;;; Core data definitions and operations on them
;; A number is an integer or a float.

;; A time-slice is a structure
;;     (make-time-slice :hours hours :minutes minutes :seconds seconds :rank rank)
;; where HOURS and MINUTES are integers, SECONDS a number, and RANK an integer.
;; Note: I don't think this merits classes yet.
(defstruct time-slice
  hours
  minutes
  seconds
  (rank 0))

;;; Rudimentary class design
;; (defclass time-slice ()
;;   ((hours :accessor hours :initarg :hours)
;;    (minutes :accessor minutes :initarg :minutes)
;;    (seconds :accessor seconds :initarg :seconds)
;;    (rank :accessor rank :initarg :rank :initform 0)))

;; (defun time-slice-equalp (time-slice1 time-slice2)
;;   (and (eql (hours time-slice1) (hours time-slice2))
;;        (eql (minutes time-slice1) (minutes time-slice2))
;;        (eql (seconds time-slice1) (seconds time-slice2))
;;        (eql (rank time-slice1) (rank time-slice2))))

;; (let ((foo-ts (make-instance 'time-slice :hours 0 :minutes 0 :seconds 0.12))
;;       (bar-ts (make-instance 'time-slice :hours 0 :minutes 0 :seconds 0.12)))
;;   (time-slice-equalp foo-ts bar-ts))

;; process-time-slice : time-slice -> ?
;; (defun process-time-slice (time-slice)
;;   ... (time-slice-hours time-slice) ...
;;   ... (time-slice-minutes time-slice) ...
;;   ... (time-slice-seconds time-slice) ...
;;   ... (time-slice-rank time-slice) ...)

;; time-slice-as-seconds : time-slice -> number
(defun time-slice-as-seconds (time-slice)
  "Determine the number of seconds in TIME-SLICE."
  (check-type time-slice time-slice)
  (+ (time-slice-seconds time-slice)
     (* 60 (time-slice-minutes time-slice))
     (* 3600 (time-slice-hours time-slice))))
;; Tests:
(test null-time-slice-zero-seconds
  (is-true (eql (time-slice-as-seconds
		 (make-time-slice :hours 0 :minutes 0 :seconds 0))
		0)))

(test 1s-time-slice-1-second
  (is-true (eql (time-slice-as-seconds
		 (make-time-slice :hours 0 :minutes 0 :seconds 1))
		1)))

(test 1ms-time-slice-1-thousandth-second
  (is-true (eql (time-slice-as-seconds
		 (make-time-slice :hours 0 :minutes 0 :seconds 0.001))
		0.001)))

(test 1min-time-slice-60-seconds
  (is-true (eql (time-slice-as-seconds
		 (make-time-slice :hours 0 :minutes 1 :seconds 0))
		60)))

(test 1hour-time-slice-3600-seconds
  (is-true (eql (time-slice-as-seconds
		 (make-time-slice :hours 1 :minutes 0 :seconds 0))
		3600)))

(test 1h1m1s-time-slice-3661-seconds
  (is-true (eql (time-slice-as-seconds
		 (make-time-slice :hours 1 :minutes 1 :seconds 1))
		3661)))

;; seconds-as-time-slice : number -> time-slice
(defun seconds-as-time-slice (secs)
  "Make a time-slice out of the given number of seconds, SECS."
  (check-type secs bu:nonnegative-real-number)
  (multiple-value-bind (hours rsecs)
      (floor secs 3600)
    (multiple-value-bind (minutes rrsecs)
	(floor rsecs 60)
      (make-time-slice :hours hours :minutes minutes :seconds rrsecs))))
;; Tests:
(test 0seconds-null-time-slice
  (is-true (equalp (seconds-as-time-slice 0)
		   (make-time-slice :hours 0 :minutes 0 :seconds 0))))

(test 1second-1s-time-slice
  (is-true (equalp (seconds-as-time-slice 1)
		   (make-time-slice :hours 0 :minutes 0 :seconds 1))))

(test 1millisecond-1ms-time-slice
  (is-true (equalp (seconds-as-time-slice 0.001)
		   (make-time-slice :hours 0 :minutes 0 :seconds 0.001))))

(test 60s-1m-time-slice
  (is-true (equalp (seconds-as-time-slice 60)
		   (make-time-slice :hours 0 :minutes 1 :seconds 0))))

(test 3600s-1h-time-slice
  (is-true (equalp (seconds-as-time-slice 3600)
		   (make-time-slice :hours 1 :minutes 0 :seconds 0))))

(test 3661s-1h1m1s-time-slice
  (is-true (equalp (seconds-as-time-slice 3661)
		   (make-time-slice :hours 1 :minutes 1 :seconds 1))))

;; difference : time-slice time-slice -> time-slice
(defun difference (p q)
  "Make a time-slice representing the difference in time between time-slices P and Q.
Assume P > Q."
  (check-type p time-slice)
  (check-type q time-slice)
  (seconds-as-time-slice (- (time-slice-as-seconds p)
			    (time-slice-as-seconds q))))
;; Tests:
(test null-difference
  (is-true (equalp (difference
		    (make-time-slice :hours 01 :minutes 24 :seconds 36)
		    (make-time-slice :hours 01 :minutes 24 :seconds 36))
		   (make-time-slice :hours 0 :minutes 0 :seconds 0))))

(test 1s-difference
  (is-true (equalp (difference
		    (make-time-slice :hours 01 :minutes 24 :seconds 36)
		    (make-time-slice :hours 01 :minutes 24 :seconds 35))
		   (make-time-slice :hours 0 :minutes 0 :seconds 1))))

(test 1min-difference
  (is-true (equalp (difference
		    (make-time-slice :hours 01 :minutes 24 :seconds 36)
		    (make-time-slice :hours 01 :minutes 23 :seconds 36))
		   (make-time-slice :hours 0 :minutes 1 :seconds 0))))

(test 1hr-difference
  (is-true (equalp (difference
		    (make-time-slice :hours 01 :minutes 24 :seconds 36)
		    (make-time-slice :hours 00 :minutes 24 :seconds 36))
		   (make-time-slice :hours 1 :minutes 0 :seconds 0))))

;; A triplet is a list
;;    (list hours minutes seconds)
;; where HOURS and MINUTES are integers, and SECONDS is a number.

;; process-triplet : triplet -> ?
;; (defun process-triplet (triplet)
;;   ... (first triplet) ...
;;   ... (second triplet) ...
;;   ... (third triplet) ...)

;; triplet-as-time-slice : triplet -> time-slice
(defun triplet-as-time-slice (triplet)
  "Make a time-slice representing the time encoded in TRIPLET."
  (let ((hours (first triplet))
	(minutes (second triplet))
	(seconds (third triplet)))
    (check-type hours bu:natural-number)
    (check-type minutes bu:natural-number)
    (check-type seconds bu:nonnegative-real-number)
    (make-time-slice :hours hours
		     :minutes minutes
		     :seconds seconds)))
;; Tests:
(test 1s-triplet-1s-time-slice
  (is-true (equalp
	    (triplet-as-time-slice (list 0 0 1))
	    (make-time-slice :hours 0 :minutes 0 :seconds 1))))

(test 1ms-triplet-1ms-time-slice
  (is-true (equalp
	    (triplet-as-time-slice (list 0 0 0.001))
	    (make-time-slice :hours 0 :minutes 0 :seconds 0.001))))

(test 1min-triplet-1min-time-slice
  (is-true (equalp
	    (triplet-as-time-slice (list 0 1 0))
	    (make-time-slice :hours 0 :minutes 1 :seconds 0))))

(test 1hr-triplet-1hr-time-slice
  (is-true (equalp
	    (triplet-as-time-slice (list 1 0 0))
	    (make-time-slice :hours 1 :minutes 0 :seconds 0))))

(test 1h-1m-1s-triplet-1h-1m-1s-time-slice
  (is-true (equalp
	    (triplet-as-time-slice (list 1 1 1))
	    (make-time-slice :hours 1 :minutes 1 :seconds 1))))

;; make-time-slice-pairs : list-of-triplet-pairs -> list-of-time-slice-pairs
(defun make-time-slice-pairs (triplet-pairs)
  "Make a ranked list of time-slice pairs from TRIPLET-PAIRS."
  (loop with time-slice-pairs = ()
	for triplet-pair in triplet-pairs
	for i below (length triplet-pairs)
	do
	   (let ((first-time-slice (triplet-as-time-slice (first triplet-pair)))
		 (second-time-slice (triplet-as-time-slice (second triplet-pair))))
	     (setf (time-slice-rank first-time-slice) i)
	     (setf (time-slice-rank second-time-slice) i)
	     (push (list first-time-slice second-time-slice) time-slice-pairs))
	finally (return (nreverse time-slice-pairs))))
;; Tests:
(test singlet-pair-1-rank-slice
  (is-true (equalp (make-time-slice-pairs (list (list (list 00 01 02) (list 00 03 04))))
		   (list (list (make-time-slice :hours 00 :minutes 01 :seconds 02 :rank 0)
			       (make-time-slice :hours 00 :minutes 03 :seconds 04 :rank 0))))))

(test doublet-pair-2-ranks-slice
  (is-true (equalp (make-time-slice-pairs (list (list (list 00 01 02) (list 00 03 04))
						(list (list 00 05 06) (list 00 07 08))))
		   (list (list (make-time-slice :hours 00 :minutes 01 :seconds 02 :rank 0)
			       (make-time-slice :hours 00 :minutes 03 :seconds 04 :rank 0))
			 (list (make-time-slice :hours 00 :minutes 05 :seconds 06 :rank 1)
			       (make-time-slice :hours 00 :minutes 07 :seconds 08 :rank 1))))))

;; A time-string is a string
;;    either "hh:mm:ss", where HH, MM, and SS are integers in [0, 59],
;;    or "hh:mm:ss.µµµ", where HH, and MM are integers in [0, 59], and the last component
;;        has an integral part SS, again in [0, 59],
;;        and a fractional part µµµ in [001, 999].

;; time-slice-as-time-string : time-slice -> time-string
(defun time-slice-as-time-string (time-slice)
  "Make a time-string corresponding to TIME-SLICE."
  (check-type time-slice time-slice)
  (let ((format-string (if (floatp (time-slice-seconds time-slice))
			   "~2,'0d:~2,'0d:~6,3,,,'0f"
			   "~2,'0d:~2,'0d:~2,'0d")))
    (format nil format-string
	    (time-slice-hours time-slice)
	    (time-slice-minutes time-slice)
	    (time-slice-seconds time-slice))))
;; Tests:
(test null-time-slice-null-time-string
  (is-true (string= (time-slice-as-time-string (make-time-slice :hours 0 :minutes 0 :seconds 0))
		    "00:00:00")))

(test 1s-time-slice-1s-time-string
  (is-true (string= (time-slice-as-time-string (make-time-slice :hours 0 :minutes 0 :seconds 1))
		    "00:00:01")))

(test 1ms-time-slice-1ms-time-string
  (is-true (string= (time-slice-as-time-string (make-time-slice :hours 0 :minutes 0 :seconds 0.001))
		    "00:00:00.001")))

(test 1min-time-slice-1min-time-string
  (is-true (string= (time-slice-as-time-string (make-time-slice :hours 0 :minutes 1 :seconds 0))
		    "00:01:00")))

(test 1hr-time-slice-1hr-time-string
  (is-true (string= (time-slice-as-time-string (make-time-slice :hours 1 :minutes 0 :seconds 0))
		    "01:00:00")))

;; seconds-as-time-string : seconds -> time-string
(defun seconds-as-time-string (seconds)
  "Make a time-string corresponding to the given SECONDS."
  (check-type seconds bu:natural-number)
  (let* ((time-slice (seconds-as-time-slice seconds))
	 (time-string (time-slice-as-time-string time-slice)))
    time-string))
;; tests:
(test 0s-0s-time-string
  (is-true (string= (seconds-as-time-string 0)
		    "00:00:00")))
(test 1s-1s-time-string
  (is-true (string= (seconds-as-time-string 1)
		    "00:00:01")))
(test 60s-1m-time-string
  (is-true (string= (seconds-as-time-string 60)
		    "00:01:00")))
(test 3600s-1h-time-string
  (is-true (string= (seconds-as-time-string 3600)
		    "01:00:00")))
(test 2657s-00h44m17s-time-string
  (is-true (string= (seconds-as-time-string 2657)
		    "00:44:17")))

;; print-seconds-as-time-string : seconds ->
(defun print-seconds-as-time-string (seconds)
  "Print the time-string corresponding to the given SECONDS."
  (check-type seconds bu:natural-number)
  (format t "~a" (seconds-as-time-string seconds)))


;;; Parameters and core functionality
;; Lisp system specific function variables
(defparameter *exit-code-function* nil
  "The function that obtains the exit code of a subprocess.")
(defparameter *run-program-function* nil
  "The function that starts a subprocess.")

;; The interfaces of CMU, SB, and Clouzure CL are similar enough to be handled
;; uniformly.  But Allegro and ECL must to handled differently.
(setf *exit-code-function*
      #+cmucl #'extensions:process-exit-code
      #+sbcl #'sb-ext:process-exit-code
      #+ccl (lambda (process) (second (multiple-value-list (ccl:external-process-status process))))
      #+ecl nil
      #+allegro nil)
(setf *run-program-function*
      #+cmucl #'extensions:run-program
      #+sbcl #'sb-ext:run-program
      #+ccl #'ccl:run-program
      #+ecl nil
      #+allegro nil)

(defparameter *ffmpeg-program* "/usr/bin/ffmpeg"
  "Full path to the installed ffmpeg(1) binary.")
(defparameter *video-format* nil
  "The video format of the intermediate and final media streams.")
(defparameter *audio-format* nil
  "The audio format of the intermediate and final media streams.")
(defparameter *video-codec* nil
  "The codec to encode the video stream in the source media file.")
(defparameter *audio-codec* nil
  "The codec to encode the audio stream in the source media file.")

(defparameter *formats-codecs*
  '(("mp4" "libx264")
    ("mp3" "libmp3lame")
    ("aac" "aac")
    ("opus" "libopus")
    ("vp9" "libvpx-vp9")))

(defparameter *source-file* nil
  "Full path to the source media file.")
(defparameter *destination-file* nil
  "Full path to the final media file.")
(defparameter *intermediate-prefix* nil
  "The common name of each intermediate slice.")
(defparameter *destination-directory* nil
  "The directory of the destination media file.")
(defparameter *timings-file* nil
  "Full path to the file containing the start and end timings as a list of triplet pairs.")
(defparameter *concat-file* nil
  "The base name of the file containing intermediate file names suitable to the 'concat' filter of ffmpeg(1).")
(defparameter *concat-file-path* nil
  "Full path to the file containing intermediate file names.")

(defparameter *slices-count* nil
  "Number of slices of the source media file.")

(defparameter *video-fps* 30
  "The frame rate of the video.  A hack until we learn how to get it programmatically.")

;; make-processor-core-count-string : -> string
;; (defun make-processor-core-count-string ()
;;   "Return a string with a count of the available processor cores."
;;   (make-string 1 :initial-element
;; 	       (read-char (sb-ext:process-output
;; 			   (sb-ext:run-program "nproc" nil :search t :output :stream))
;; 			  nil)))
;; ;; Tests:
;; (deftest equilibrium-has-4-cores
;;     (string= (make-processor-core-count-string)
;; 	     "4")
;;   t)

;; lookup-video-codec : string -> string
(defun lookup-video-codec (video-format)
  "Look up the video codec corresponding to the VIDEO-FORMAT."
  (check-type video-format string)
  (second (assoc video-format *formats-codecs* :test #'equal)))
;; Tests:
(test mp4-has-codec-libx264
  (is-true (string= (lookup-video-codec "mp4")
		    "libx264")))

(test mp3-has-codec-libmp3lame
  (is-true (string= (lookup-video-codec "mp3")
		    "libmp3lame")))

(test aac-has-codec-aac
  (is-true (string= (lookup-video-codec "aac")
		    "aac")))

(test opus-has-codec-libopus
  (is-true (string= (lookup-video-codec "opus")
		    "libopus")))

(test vp9-has-codec-libvpx-vp9
  (is-true (string= (lookup-video-codec "vp9")
		    "libvpx-vp9")))

;; make-intermediate-prefix : string -> string
(defun make-intermediate-prefix (destination-file)
  "Produce the basename of DESTINATION-FILE without directories and its extension."
  (check-type destination-file string)
  (bu:file-name-without-extension (file-namestring destination-file)))
;; Tests:
(test prefix-1
  (is-true (string= (make-intermediate-prefix "/media/foo/bar.mp4")
		    "bar")))

(test prefix-2
  (is-true (string= (make-intermediate-prefix "~/foo/bar.mp4")
		    "bar")))

;; make-intermediate-file-name : integer -> string
(defun make-intermediate-file-name (n)
  "Produce the name of the intermediate file whose slice is ranked N."
  (check-type n bu:natural-number)
  (format nil "~a~a~2,'0d.~a"
	  *destination-directory*
	  *intermediate-prefix*
	  n
	  *video-format*))
;; Tests:
(test intermediate-file-rank-0
  (let* ((*destination-file* "./foo.mp4")
	 (*destination-directory* #-ecl (directory-namestring *destination-file*)
				  #+ecl (concatenate 'string "./" (directory-namestring *destination-file*)))
	 (*intermediate-prefix* (make-intermediate-prefix *destination-file*))
	 (*video-format* (bu:file-name-extension *destination-file*)))
    (is-true (string= (make-intermediate-file-name 0)
		      "./foo00.mp4"))))

(test intermediate-file-rank-1
  (let* ((*destination-file* "./bar-foo.mp4")
	 (*destination-directory* #-ecl (directory-namestring *destination-file*)
				  #+ecl (concatenate 'string "./" (directory-namestring *destination-file*)))
	 (*intermediate-prefix* (make-intermediate-prefix *destination-file*))
	 (*video-format* (bu:file-name-extension *destination-file*)))
    (is-true (string= (make-intermediate-file-name 1)
		      "./bar-foo01.mp4"))))

;; read-triplet-pairs : string -> list-of-triplet-pairs
(defun read-triplet-pairs (timings-file)
  "Read TIMINGS-FILE and read the form therein, producing the list of triplet-pairs."
  (check-type timings-file (or string pathname))
  (with-open-file (stream timings-file
			  :direction :input)
    (read stream nil)))
;; Tests:
(test triplet-pair-doublet
  (let ((*destination-directory* (directory-namestring *default-pathname-defaults*)))
    (is-true (equal (read-triplet-pairs (concatenate 'string *destination-directory* "./foo.lisp"))
		    '(((00 01 02) (00 03 04))
		      ((00 05 06) (00 07 08)))))))

;; write-concat-file : ->
(defun write-concat-file ()
  "Write *SLICES-COUNT* intermediate file names to the *CONCAT-FILE* in *DESTINATION-DIRECTORY*."
  (with-open-file (concat-stream *concat-file-path*
				 :direction :output
				 :if-exists :supersede)
    (dotimes (i *slices-count*)
      (format concat-stream "file '~a'~%" (make-intermediate-file-name i)))))
;; Tests:
(test generate-test-concat-file
  (let* ((*destination-file* "./foo.mp4")
	 (*destination-directory* (directory-namestring *destination-file*))
	 (*intermediate-prefix* (make-intermediate-prefix *destination-file*))
	 (*concat-file* "gen-concat.txt")
	 (*concat-file-path* (concatenate 'string *destination-directory* *concat-file*))
	 (*slices-count* 3)
	 (*video-format* (bu:file-name-extension *destination-file*)))
    (write-concat-file)
    (let ((return-value #-(or allegro ecl) (eql (funcall *exit-code-function*
							 (funcall *run-program-function*
								  "/usr/bin/cmp"
								  (list "./test-concat.txt"
									"./gen-concat.txt")))
						0)
			#+ecl (multiple-value-bind (stream exit-code process)
				  (ext:run-program "/usr/bin/cmp"
						   (list "./test-concat.txt"
							 "./gen-concat.txt"))
				exit-code)
			#+allegro (eql (third (multiple-value-list
					       (excl.osi:command-output "/usr/bin/cmp ./test-concat.txt ./gen-concat.txt")))
				       0)))
      (if return-value (delete-file *concat-file*))
      (is-true return-value))))

;; An ffmpeg-command-list is a list
;;    (list ffmpeg-arguments)
;; where FFMPEG-ARGUMENTS are strings, the individual command-line arguments
;; given to ffmpeg(1).
;; This is a list suitable to pass on to the RUN-PROGRAM function in CMUCL and
;; SBCL, and also in CCL.

;; make-concat-command-list : -> ffmpeg-command-list
(defun make-concat-command-list ()
  "Make an ffmpeg-command-list suitable to pass on as an ffmpeg concat command."
  (list "-f" "concat"
	"-safe" "0"
	"-i" *concat-file-path*
	"-c" "copy"
	*destination-file*))
;; Tests:
(test concat-command-string-test
  (let* ((*destination-file* "./baz-dest.mp4")
	 (*destination-directory* (directory-namestring *destination-file*))
	 (*concat-file* "test-concat.txt")
	 (*concat-file-path* (concatenate 'string *destination-directory* *concat-file*)))
    (is-true (equal (make-concat-command-list)
		    (list "-f" "concat"
			  "-safe" "0"
			  "-i" *concat-file-path*
			  "-c" "copy"
			  *destination-file*)))))

;; make-vaapi-command-list : time-slice-pair -> ffmpeg-command-list
;; (defun make-vaapi-command-list (time-slice-pair)
;;   "Make an ffmpeg-command-list that uses h264 VAAPI to slice a piece per TIME-SLICE-PAIR."
;;   (list "-threads" (make-processor-core-count-string)
;; 	"-hwaccel" "vaapi"
;; 	"-vaapi_device" "/dev/dri/renderD128"
;; 	"-c:v" "h264_vaapi"
;; 	"-vf" "'format=nv12,hwupload'"
;; 	"-i" *source-file*
;; 	"-ss" (time-slice-as-time-string (first time-slice-pair))
;; 	"-t" (time-slice-as-time-string (difference (second time-slice-pair)
;; 						    (first time-slice-pair)))
;; 	(make-intermediate-file-name (time-slice-rank (first time-slice-pair)))))

;; make-slice-command-list : time-slice-pair -> ffmpeg-command-list
(defun make-slice-command-list (time-slice-pair)
  "Make an ffmpeg-command-list to slice a piece per TIME-SLICE-PAIR."
  (let ((first-ts (first time-slice-pair))
	(second-ts (second time-slice-pair)))
    (check-type first-ts time-slice)
    (check-type second-ts time-slice)
    (list "-i" *source-file*
	  "-ss" (time-slice-as-time-string first-ts)
	  "-t" (time-slice-as-time-string (difference second-ts first-ts))
	  "-pix_fmt" "yuv420p"
	  "-c:v" *video-codec*
	  (make-intermediate-file-name (time-slice-rank first-ts)))))
;; Tests:
(test rank-0-slice-command-list
  (let* ((*source-file* "./source-foo-bar.mp4")
	 (*destination-file* "./dest-foo-bar.mp4")
	 (*destination-directory* #-ecl (directory-namestring *destination-file*)
				  #+ecl (concatenate 'string "./" (directory-namestring *destination-file*)))
	 (*intermediate-prefix* (make-intermediate-prefix *destination-file*))
	 (*video-format* (bu:file-name-extension *destination-file*))
	 (*video-codec* (lookup-video-codec *video-format*)))
    (is-true (equalp
	      (make-slice-command-list
	       (list (make-time-slice :hours 00 :minutes 01 :seconds 02 :rank 0)
		     (make-time-slice :hours 00 :minutes 03 :seconds 04 :rank 0)))
	      (list "-i" *source-file*
		    "-ss" "00:01:02"
		    "-t" "00:02:02"
		    "-pix_fmt" "yuv420p"
		    "-c:v" *video-codec*
		    "./dest-foo-bar00.mp4")))))

(test rank-2-slice-command-list
  (let* ((*source-file* "./source-foo-bar.mp4")
	 (*destination-file* "./dest-foo-bar.mp4")
	 (*destination-directory* #-ecl (directory-namestring *destination-file*)
				  #+ecl (concatenate 'string "./" (directory-namestring *destination-file*)))
	 (*intermediate-prefix* (make-intermediate-prefix *destination-file*))
	 (*video-format* (bu:file-name-extension *destination-file*))
	 (*video-codec* (lookup-video-codec *video-format*)))
    (is-true (equalp
	      (make-slice-command-list
	       (list (make-time-slice :hours 00 :minutes 05 :seconds 06 :rank 2)
		     (make-time-slice :hours 00 :minutes 07 :seconds 08 :rank 2)))
	      (list "-i" *source-file*
		    "-ss" "00:05:06"
		    "-t" "00:02:02"
		    "-pix_fmt" "yuv420p"
		    "-c:v" *video-codec*
		    "./dest-foo-bar02.mp4")))))


;;; Subprocess interface -- main functions.
;; call-ffmpeg-with-log : string [boolean] -> number
(defun call-ffmpeg-with-log (log-file ffmpeg-arguments)
  "Execute *FFMPEG-PROGRAM* with FFMPEG-ARGUMENTS and log the output to LOG-FILE."
  (check-type log-file (or string pathname))
  #+allegro (progn (when (probe-file log-file)
		     (delete-file log-file))
		   (third (multiple-value-list
			   (excl.osi:command-output
			    (format nil "~a ~{ ~a~}" *ffmpeg-program* ffmpeg-arguments)
			    :whole t
			    :output-file log-file))))
  #+ecl (multiple-value-bind (stream exit-code process)
	    (ext:run-program *ffmpeg-program* ffmpeg-arguments
			     :output log-file
			     :if-output-exists :supersede)
	  exit-code)
  #-(or allegro ecl) (funcall *exit-code-function*
			      (funcall *run-program-function*
				       *ffmpeg-program* ffmpeg-arguments
				       :output log-file
				       :if-output-exists :supersede)))


;; cut-slice : time-slice-pair -> boolean
(defun cut-slice (time-slice-pair)
  "Execute *FFMPEG-PROGRAM* to cut an intermediate slice out of *SOURCE-FILE* per TIME-SLICE-PAIR.  Return the exit code."
  (let* ((first-ts (first time-slice-pair))
	 (first-ts-rank (time-slice-rank first-ts)))
    (check-type first-ts time-slice)
    (if (probe-file (make-intermediate-file-name first-ts-rank))
	t
	(let* ((cut-log-name (format nil "cut-~2,'0d" first-ts-rank))
	       (cut-log-file (format nil "~a~a-output.log"
				     *destination-directory*
				     cut-log-name))
	       (slice-command-list (make-slice-command-list time-slice-pair))
	       (ffmpeg-arguments slice-command-list)
	       (exit-code (call-ffmpeg-with-log cut-log-file ffmpeg-arguments)))
	  (and (zerop exit-code) (delete-file cut-log-file))))))
;; Tests:
;; (deftest ogg-as-mp4_rank0
;;     (let* ((*ffmpeg-program* "/usr/bin/ffmpeg")
;; 	   (*source-file* "./video.ogg")
;; 	   (*destination-file* "./2-mins.mp4")
;; 	   (*destination-directory* (directory-namestring *destination-file*))
;; 	   (*intermediate-prefix* (make-intermediate-prefix *destination-file*))
;; 	   (*video-format* (bu:file-name-extension *destination-file*))
;; 	   (*video-codec* (lookup-video-codec *video-format*)))
;;       (cut-slice (list (make-time-slice :hours 00 :minutes 01 :seconds 02 :rank 0)
;; 		       (make-time-slice :hours 00 :minutes 03 :seconds 02 :rank 0))))
;;   t)

;; (deftest ogg-as-mp4_rank2
;;   (let* ((*ffmpeg-program* "/usr/bin/ffmpeg")
;; 	   (*source-file* "./video.ogg")
;; 	   (*destination-file* "./2-later-mins.mp4")
;; 	   (*destination-directory* (directory-namestring *destination-file*))
;; 	   (*intermediate-prefix* (make-intermediate-prefix *destination-file*))
;; 	   (*video-format* (bu:file-name-extension *destination-file*))
;; 	   (*video-codec* (lookup-video-codec *video-format*)))
;;     (cut-slice (list (make-time-slice :hours 00 :minutes 04 :seconds 05 :rank 2)
;; 		       (make-time-slice :hours 00 :minutes 05 :seconds 35 :rank 2))))
;; t)

;; ;; all-t-p : list-of-boolean -> boolean
;; (defun all-t-p (booleans)
;;   "Determine whether all the values in BOOLEANS are T."
;;   (every #'identity booleans))
;; ;; Tests:
;; (test all-bools-t
;;   (is-true (all-t-p (list t t t t))))

;; (test some-bools-nil
;;   (is-false (all-t-p (list nil t nil t))))

;; cut-slices : list-of-time-slice-pairs -> boolean
(defun cut-slices (time-slice-pairs)
  "Execute *FFMPEG-PROGRAM* to cut intermediate slices out of *SOURCE-FILE* per TIME-SLICE-PAIRS."
  ;; (loop with slice-cut-results = ()
  ;;    with slice-cut-p = nil
  ;;    for time-slice-pair in time-slice-pairs
  ;;    do
  ;;      (setf slice-cut-p (cut-slice time-slice-pair))
  ;;      (push slice-cut-p slice-cut-results)
  ;;    finally (return (all-t-p slice-cut-results)))
  ;; (every #'identity (mapcar #'cut-slice time-slice-pairs))
  (every #'cut-slice time-slice-pairs))
;; Tests:
;; (deftest ogg-as-2mp4s
;;     (let* ((*ffmpeg-program* "/usr/bin/ffmpeg")
;; 	   (*source-file* "./video.ogg")
;; 	   (*destination-file* "./2-sep.mp4")
;; 	   (*destination-directory* (directory-namestring *destination-file*))
;; 	   (*intermediate-prefix* (make-intermediate-prefix *destination-file*))
;; 	   (*video-format* (bu:file-name-extension *destination-file*))
;; 	   (*video-codec* (lookup-video-codec *video-format*)))
;;       (cut-slices (list (list (make-time-slice :hours 00 :minutes 04 :seconds 06 :rank 0)
;; 			      (make-time-slice :hours 00 :minutes 05 :seconds 06 :rank 0))
;; 			(list (make-time-slice :hours 00 :minutes 09 :seconds 10 :rank 1)
;; 			      (make-time-slice :hours 00 :minutes 10 :seconds 10 :rank 1)))))
;;   t)

;; combine-slices : -> boolean
(defun combine-slices ()
  "Execute ffmpeg to combine the already cut SLICES-COUNT slices listed in CONCAT-FILE."
  (write-concat-file)
  (let* ((concat-log-name "concat-log")
	 (concat-log-file (format nil "~a~a-output.log"
				  *destination-directory*
				  concat-log-name))
	 (concat-command-list (make-concat-command-list))
	 (ffmpeg-arguments concat-command-list)
	 (exit-code (call-ffmpeg-with-log concat-log-file ffmpeg-arguments)))
    (and (zerop exit-code) (delete-file concat-log-file))))
;; Tests:
;; (deftest two-separate-1min-slices-as-one-2min-mp4
;;     (let* ((*ffmpeg-program* "/usr/bin/ffmpeg")
;; 	   (*source-file* "./video.ogg")
;; 	   (*destination-file* "./2-sep.mp4")
;; 	   (*destination-directory* (directory-namestring *destination-file*))
;; 	   (*intermediate-prefix* (make-intermediate-prefix *destination-file*))
;; 	   (*concat-file* "2-sep-concat.txt")
;;         (*concat-file-path* (concatenate 'string *destination-directory* *concat-file*))
;; 	   (*video-format* (bu:file-name-extension *destination-file*))
;; 	   (*video-codec* (lookup-video-codec *video-format*))
;; 	   (*slices-count* 2))
;; 	(combine-slices))
;;   t)

;; delete-concat-file : -> t
(defun delete-concat-file ()
  "Delete the *CONCAT-FILE* associated with *DESTINATION-FILE*."
  (delete-file *concat-file-path*))
;; Tests:
(test remove-concat-file
  (let* ((*destination-file* "./foo.mp4")
	 (*destination-directory* (directory-namestring *destination-file*))
	 (*concat-file* "foo-concat.txt")
	 (*concat-file-path* (concatenate 'string *destination-file* *concat-file*)))
    (with-open-file (temp *concat-file-path*
			  :direction :output
			  :if-exists :supersede)
      (format temp "foo00.mp4~%")
      (format temp "foo01.mp4~%"))
    (delete-concat-file)
    (is-false (probe-file *concat-file-path*))))

;; delete-intermediate-files : -> t
(defun delete-intermediate-files ()
  "Delete each intermediate slice associated with *DESTINATION-FILE*."
  (dotimes (i *slices-count* t)
    (let ((intermediate-file-name (make-intermediate-file-name i)))
      (when (probe-file intermediate-file-name)
	(delete-file intermediate-file-name)))))

;; ensure-destination-clear : -> t
(defun ensure-destination-clear ()
  "Delete the *DESTINATION-FILE* if it exists."
  (when (probe-file *destination-file*)
    (delete-file *destination-file*))
  t)

;; make-frame-saving-command-list : triplet -> ffmpeg-command-list
(defun make-frame-saving-command-list (triplet frame-name)
  "Make an ffmpeg-command-list to save a frame named FRAME-NAME at time represented by TRIPLET."
  (let ((time-slice (triplet-as-time-slice triplet)))
    (list "-ss" (time-slice-as-time-string time-slice)
	  "-i" *source-file*
	  "-vframes" "1"
	  "-q:v" "2"
	  frame-name)))
;; Tests:
(test 1h2m3s-triplet-foobar-jpg
  (let ((*source-file* "baz-quux.mp4"))
    (is-true (equalp (make-frame-saving-command-list '(01 02 03) "foobar.jpg")
		     (list "-ss" (time-slice-as-time-string (triplet-as-time-slice '(01 02 03)))
			   "-i" *source-file*
			   "-vframes" "1"
			   "-q:v" "2"
			   "foobar.jpg")))))

;; grab-screenshot : triplet string -> boolean
(defun grab-screenshot (triplet name)
  "Save a screenshot named NAME of *SOURCE-FILE* at the time represented by TRIPLET."
  (check-type name string)
  (let* ((frame-saving-command-list (make-frame-saving-command-list triplet name))
	 (frame-log-file (format nil "~a~a-frame-out.log"
				 *destination-directory*
				 name))
	 (exit-code (call-ffmpeg-with-log frame-log-file frame-saving-command-list)))
    (and (zerop exit-code) (delete-file frame-log-file))))
;; Tests:
(test 5min-frame-5min.jpg
  (let* ((*ffmpeg-program* "/usr/bin/ffmpeg")
	 (*source-file* "./video.ogg")
	 (*destination-directory* "./")
	 (shot-name "5min.jpg")
	 (return-value (grab-screenshot '(00 05 00) shot-name)))
    (if return-value (delete-file shot-name))
    (is-true return-value)))

;; A named-triplet is a pair
;;    (list triplet name)
;; where TRIPLET is a triplet, and NAME is a string.
;; Examples:
;; (list '(00 01 23) "first.jpg")
;; (list '(01 22 54) "table-cap.jpg")

;; grab-screenshots : list-of-named-triplets -> boolean
(defun grab-screenshots (named-triplets)
  "Save screenshots from *SOURCE-FILE* at times with corresponding names represented by NAMED-TRIPLETS."
  ;; (loop with all-true = t
  ;;    for (triplet name) in named-triplets
  ;;    do (if (eq (grab-screenshot triplet name) nil)
  ;; 	    (setf all-true nil))
  ;;    finally (return all-true))
  (every #'(lambda (named-triplet)
	     (destructuring-bind (triplet name) named-triplet
	       (grab-screenshot triplet name)))
	 named-triplets))
;; Tests:
(test 1m-2m-3m-4m-5m-frames-1min-2min-3min-4min-5min
  (let* ((*ffmpeg-program* "/usr/bin/ffmpeg")
	 (*source-file* "./video.ogg")
	 (*destination-directory* "./")
	 (named-triplets '(((00 01 00) "first.jpg")
			   ((00 02 00) "second.jpg")
			   ((00 03 00) "third.jpg")
			   ((00 04 00) "fourth.jpg")
			   ((00 05 00) "fifth.jpg")))
	 (return-value (grab-screenshots named-triplets)))
    (if return-value
	(loop for (triplet shot-name) in named-triplets
	      do (delete-file shot-name)))
    (is-true return-value)))

;; save-named-screenshots : filename filename -> boolean
(defun save-named-screenshots (&key source-file shots-file)
  "Read SHOTS-FILE, a series of named-triplets, and save screenshots from SOURCE-FILE at listed times."
  (check-type source-file (or string pathname))
  (check-type shots-file (or string pathname))
  (with-open-file (shottimes shots-file
                             :direction :input)
    (loop with all-saved = t
	  for named-triplet = (read shottimes nil nil)
	  ;; for i from 1
	  while named-triplet
	  do (destructuring-bind (triplet name) named-triplet
	       (let ((*source-file* source-file))
		 ;; (format t "~a    trip: ~a    name: ~a~%" i triplet name)
		 (if (eq (grab-screenshot triplet name) nil)
		     (setf all-saved nil))))
	  finally (return all-saved))))

;; speed-video : string speed-video [boolean] -> ?
(defun speed-video (filename speed-factor &optional (has-audio-p nil))
  "Speed up the video named by FILENAME by the factor SPEED-FACTOR, and append \"-sped\" to the sped video's name."
  (check-type filename (or string pathname))
  (check-type speed-factor bu:nonnegative-real-number)
  (let* ((sped-video-base-name (concatenate 'string
					    (bu:file-name-without-extension filename)
					    "-sped"))
	 (sped-video-extension (bu:file-name-extension filename))
	 (sped-video-full-name (format nil "~a.~a" sped-video-base-name sped-video-extension)))
    (when (probe-file sped-video-full-name)
      (delete-file sped-video-full-name))
    (let* ((sped-log-file (format nil "~a.log" sped-video-base-name))
	   (new-pts-factor (/ speed-factor))
	   ;; (speeding-command-list (list "-i" filename "-c:a" "aac" "-c:v" (lookup-video-codec sped-video-extension)
	   ;; 				"-r" (format nil "~a" (* speed-factor *video-fps*))
	   ;; 				"-filter:v"
	   ;; 				(format nil "'setpts=~a*PTS'" new-pts-factor)
	   ;; 				sped-video-full-name))
	   (speeding-command-list (if has-audio-p
				      (list "-i" filename
					    "-filter_complex"
					    (format nil "[0:v]setpts=~a*PTS[v];[0:a]atempo=~a[a]"
						    new-pts-factor
						    speed-factor)
					    "-map" "[v]"
					    "-map" "[a]"
					    sped-video-full-name)
				      (list "-i" filename
					    "-filter_complex"
					    (format nil "[0:v]setpts=~a*PTS[v]"
						    new-pts-factor)
					    "-map" "[v]"
					    sped-video-full-name)))
	   (exit-code (call-ffmpeg-with-log sped-log-file speeding-command-list)))
      (and (zerop exit-code)
	   (delete-file sped-log-file)))))

;; slice-media : string string string -> ?
(defun slice-media (&key source-file destination-file timings-file (keep-intermediate-files nil) (speed-factor nil))
  "Slice SOURCE-FILE into pieces per TIMINGS-FILE; combine them into DESTINATION-FILE.
Requires ffmpeg(1) to be installed on the system."
  (check-type source-file (or string pathname))
  (check-type destination-file (or string pathname))
  (check-type timings-file (or string pathname))
  (let* ((triplet-pairs (read-triplet-pairs timings-file))
	 (time-slice-pairs (make-time-slice-pairs triplet-pairs)))
    ;; Set parameters.
    (setf *slices-count* (length triplet-pairs))
    (setf *timings-file* timings-file)
    (setf *destination-file* destination-file)
    (setf *destination-directory* (directory-namestring *destination-file*))
    (setf *source-file* source-file)
    (setf *intermediate-prefix* (make-intermediate-prefix *destination-file*))
    (setf *concat-file* (concatenate 'string *intermediate-prefix* "-concat.txt"))
    (setf *concat-file-path* (concatenate 'string *destination-directory* *concat-file*))
    (setf *video-format* (bu:file-name-extension *destination-file*))
    (setf *video-codec* (lookup-video-codec *video-format*))

    (and
     (ensure-destination-clear)
     (cut-slices time-slice-pairs)
     (combine-slices)
     (delete-concat-file)
     (unless keep-intermediate-files
       (delete-intermediate-files))
     (or (when speed-factor
	   (speed-video *destination-file* speed-factor))
	 t))))
;; Tests:
(test 2oggs-as-mp4
  (let ((*source-directory* (directory-namestring *default-pathname-defaults*))
	(*destination-directory* (directory-namestring *default-pathname-defaults*)))
    (and (is-true (slice-media :source-file (concatenate 'string *source-directory* "./video.ogg")
			       :destination-file (concatenate 'string *destination-directory* "./2-sep.mp4")
			       :timings-file (concatenate 'string *destination-directory* "./2-sep-timings.lisp")))
	 (is-true (delete-file *destination-file*)))))
